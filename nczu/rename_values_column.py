import pandas as pd


csv_input = pd.read_csv('events_output_2.csv')
csv_input['discharge_code'] = csv_input['discharge_code'].replace(
    {'death': 'Помер(ла)', 'discharge_better': 'Виписаний з одужанням', 'discharge_recovery': 'Виписаний з одужанням',
     'discharge_healthy': 'Здоровий(а)', 'discharge_no_change': 'Без змін','discharge_worse': 'Виписаний з погіршенням',
     'left_by_patient ': 'Пацієнт залишив заклад охорони здоровя всупереч медичним рекомендаціям',
     'statistic_discharge ': 'Статистична виписка','transfer_general ': 'Переведено в інший ЗОЗ',})
csv_input.to_csv('events_output_3.csv', index=False)
